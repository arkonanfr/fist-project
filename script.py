import os
import sys
import csv
import json
import math
import argparse

def parse_fist_file(file_path):
    """
    Parse the FiST file and extract relevant information.
    """
    user_stats = {}
    total_files = 0
    total_space = 0

    with open(file_path, 'r') as f:
        for line in f:
            fields = line.strip().split(':')
            if len(fields) != 10:
                continue

            _, _, _, uid, _, size, _, _, _, _ = fields
            uid = int(uid)
            size = int(size)
            
            if uid not in user_stats:
                user_stats[uid] = {'files': 0, 'space': 0, 'sizes': []}
            
            user_stats[uid]['files'] += 1
            user_stats[uid]['space'] += size
            user_stats[uid]['sizes'].append(size)

            total_files += 1
            total_space += size

    return user_stats, total_files, total_space

# probleme de moyenne ET/OU mediane comparé au fichier .fist
def calculate_statistics(sizes):
    """
    Calculate average and median from a list of sizes.
    """
    if not sizes:
        return 0, 0
    
    avg_size = sum(sizes) / len(sizes)
    sorted_sizes = sorted(sizes)
    median_size = sorted_sizes[len(sorted_sizes) // 2]
    return avg_size, median_size

def generate_summary(fist_file):
    """
    Generate summary from the FiST file.
    """
    user_stats, total_files, total_space = parse_fist_file(fist_file)
    summary = []

    for uid, stats in user_stats.items():
        files = stats['files']
        space = stats['space'] / (1024 * 1024)  # Convert to MB
        avg_size, median_size = calculate_statistics(stats['sizes'])
        dirs = 1  # j'ai mit 1 mais je crois qu'il n'y a pas 1 directory par user à chaque fois
        symlinks = 0  # No information about symlinks in FiST file
        
        summary.append(f"{uid}: {files} files, {space:.2f} MB, avg: {avg_size:.2f} (MB), median: {median_size:.2f} (MB), dirs: {dirs}, symlinks: {symlinks}")

    # Add total summary
    total_space_mb = total_space / (1024 * 1024)  # Convert to MB
    total_avg_size, total_median_size = calculate_statistics([stat['space'] for stat in user_stats.values()])
    total_summary = f"*Total: {total_files} files, {total_space_mb:.2f} MB, avg: {total_avg_size:.2f} (MB), median: {total_median_size:.2f} (MB)"
    summary.append(total_summary)

    return summary

def export_to_txt(summary):
    """
    Export summary to TXT format.
    """
    for line in summary:
        print(line)

def export_to_json(summary):
    """
    Export summary to JSON format.
    """
    json_output = {
        "summary": summary
    }
    print(json.dumps(json_output, indent=4))

def export_to_csv(summary):
    """
    Export summary to CSV format.
    """
    writer = csv.writer(sys.stdout)
    writer.writerow(['User', 'Number of Files', 'Total Space (MB)', 'Average File Size (MB)', 'Median File Size (MB)', 'Dirs', 'Symlinks'])
    for line in summary[:-1]:  # Exclude total summary
        uid, files, space, avg_size, median_size, dirs, symlinks = line.split(': ')
        writer.writerow([uid, files.split()[0], space.split()[0], avg_size.split()[1], median_size.split()[1], dirs.split()[1], symlinks.split()[1]])

def main():
    parser = argparse.ArgumentParser(description='Process FiST files and generate summary.')
    parser.add_argument('fist_file', metavar='FIST_FILE', type=str, help='the FiST file to process')
    parser.add_argument('--json', action='store_true', help='export to JSON format')
    parser.add_argument('--csv', action='store_true', help='export to CSV format')
    args = parser.parse_args()

    fist_file = args.fist_file
    if not os.path.exists(fist_file):
        print("Error: FiST file not found.")
        sys.exit(1)

    summary = generate_summary(fist_file)

    if args.json:
        export_to_json(summary)
    elif args.csv:
        export_to_csv(summary)
    else:
        export_to_txt(summary)

if __name__ == "__main__":
    main()
