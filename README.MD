
# Objectif du script :


Vous devez écrire un script qui permet d’obtenir un résumé du contenu des fichiers FiST (small.fist et medium.fist), qui soit aussi proche que possible de ce qui est fourni dans les fichiers de résultat (small.txt et medium.txt)

Par exemple small.txt (résultat de l’analyse de small.fist)
contient (extrait) :
```
*User: number of files, total space, average file size, median file size, dirs, symlinks
14641: 0 files, 0.00 MB, avg: 0.00 (MB), median: 0.00 (MB), dirs: 1, symlinks: 0
14352: 104 files, 4.52 MB, avg: 0.04 (MB), median: 0.03 (MB), dirs: 8, symlinks: 0
[...]
14829: 53311 files, 2747.18 MB, avg: 0.05 (MB), median: 0.00 (MB), dirs: 2979, symlinks: 2
14261: 58765 files, 6925.44 MB, avg: 0.12 (MB), median: 0.00 (MB), dirs: 5984, symlinks: 12
*Total: 115603 files, 10682.39 MB, avg: 0.09 (MB), median: 0.00 (MB), dirs: 9074, symlinks: 20
```


**Objectif**
 Premier temps : 
  obtenir le décompte par utilisateur (plus des totaux, éventuellement sans la médiane globale) pour :
  - Le nombre de fichiers
  - L’espace occupé par les fichiers
  - La moyenne et la médiane des tailles de fichiers
  - Le nombre de répertoires
  - Le nombre de liens symboliques
 
 **Les identifiants utilisateurs ne peuvent pas être résolus et sont à
 utiliser sous forme numérique**
 
 Dans un **==second temps==** :
 - Faites une copie/seconde version de votre outil qui produit du JSON Ou donnez à votre outil la possibilité de produire les deux types de résultats (JSON & texte)
 - La structure des fichiers JSON à produire est aussi fournie, par exemple, medium.json pour le fichier medium.fist

 Dans un **==troisième temps==** :
 - Modifiez votre script pour ajouter des histogrammes de distribution des tailles de fichiers (uniquement) par utilisateur et globalement (dans la sortie JSON uniquement)
 - C'est-à-dire, pour un utilisateur donné, combien de fichiers avec une taille entre (par exemple) 4 et 8 Kio ou entre 16 et 32 Mio.
 - Utiliser les puissances de 2 consécutives comme critère de tailles :
 ```
 (0, 1, 2, octets, [3-4] octets, [5-8] octets, [9-16] octets, [17-32]
 octets, [33-64], [65-128], ..., [1025-2048], [2049-4096], ..., [65537-
 131072], ..., [1 Mio+1-2 Mio], ..., [1 Gio+1-2 Gio], ..., etc.)
 ```
S'il n'y a pas de fichiers dans un intervalle de taille, cet intervalle ==**ne doit pas**== être affiché dans le résultat
 Exemple : `medium-histo.json` pour le fichier `medium.fist`.


## Format FiST

Les fichiers FiST contiennent les métadonnées de systèmes de
fichiers dans un format de type CSV (sans en-tête), une ligne par
objet du système de fichiers, 10 champs par ligne avec : comme
séparateur

 Les 10 champs de chaque ligne sont :
 ```
 blocks:mode:hardlinks:uid:gid:size:mtime:atime:ctime:name
 ```

> - `blocks` : nombre de blocs de 1024 octets occupés sur disque
> - `mode` : type et permissions de l’objet (voir transparent 6)
> - `hardlinks` : nombre de liens « durs » vers cet objet
> - `uid` : identifiant numérique du propriétaire de l’objet
> - `gid` : identifiant numérique du groupe de l’objet
> - `size` : taille de l’objet en octet
> - `mtime` : date de dernière modification de l’objet
> - `atime` : date de dernier accès à l’objet
> - `ctime` : date de dernier changement de métadonnées de l’objet
> - `name` : nom de l’objet
